#include <type_traits>
#include <iostream>
#include <string>

template <typename T, typename = typename std::enable_if<std::is_class<T>::value>::type>
T const &greater(T const &a, T const &b)
{
    std::cout << "T const & ";
    return a > b ? a : b;
}

template <typename T, typename = typename std::enable_if<!std::is_class<T>::value>::type>
T greater(T a, T b)
{
    std::cout << "T ";
    return a > b ? a : b;
}

int main()
{
    std::cout << greater(std::string{"A"}, std::string{"B"}) << std::endl;
    std::cout << greater('A', 'B') << std::endl;
}
