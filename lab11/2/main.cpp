#include <stdexcept>
#include <string>

struct NoCheckIndexing
{
    template <typename V>
    static void index(V &, size_t &) {}
};

struct ThrowingIndexing
{
    template <typename V>
    static void index(V &v, size_t &idx)
    {
        if (idx >= v.size)
            throw std::out_of_range{""};
    }
};

struct CyclicIndexing
{
    template <typename V>
    static void index(V &v, size_t &idx)
    {
        idx %= v.size;
    }
};

struct StretchingIndexing
{
    template <typename V>
    static void index(V &v, size_t &idx)
    {
        if (idx >= v.size)
            v.resize(idx + 1);
    }
};

template <typename T, class IndexingStrategy = NoCheckIndexing>
class Vector
{
    friend IndexingStrategy;

private:
    size_t size;
    T *data;

public:
    explicit Vector(size_t size)
        : size{size},
          data{new T[size]}
    {
    }
    Vector(Vector const &) = delete;            /* lusta */
    Vector &operator=(Vector const &) = delete; /* lusta */
    ~Vector()
    {
        delete[] data;
    }

    void resize(size_t new_size)
    {
        auto new_data = new T[new_size];
        for (size_t i = 0; i < size; ++i)
        {
            try
            {
                new_data[i] = std::move_if_noexcept(data[i]);
            }
            catch (...)
            {
                delete[] new_data;
                throw;
            }
        }
        delete[] data;
        data = new_data;
    }

    T &operator[](size_t idx)
    {
        IndexingStrategy::index(*this, idx);
        return data[idx];
    }
};

int main()
{
    Vector<std::string, NoCheckIndexing> v1(10);
    Vector<std::string, CyclicIndexing> v2(10);
    Vector<std::string, ThrowingIndexing> v3(10);
    Vector<std::string, StretchingIndexing> v4(10);
    v1[0];
    v2[0];
    v3[0];
    v4[0];
}
