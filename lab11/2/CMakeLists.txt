cmake_minimum_required(VERSION 3.0.0)
project(lab11_2 VERSION 0.1.0)

set(CMAKE_CXX_STANDARD 23)

add_executable(lab11_2 main.cpp)

target_compile_options(lab11_2 PRIVATE -Werror -Wall -Wextra)
