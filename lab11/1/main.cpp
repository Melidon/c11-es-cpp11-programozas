#include <cstring>
#include <cctype>
#include <iostream>

struct DefaulsCharTraits
{
    using char_t = char;

    static bool equal(char c1, char c2)
    {
        return c1 == c2;
    }

    static void copy(char_t *dest, const char_t *src)
    {
        strcpy(dest, src);
    }
};

struct SameCaseTraits
{
    using char_t = char;

    static bool equal(char c1, char c2)
    {
        return tolower(c1) == tolower(c2);
    }

    static void copy(char_t *dest, const char_t *src)
    {
        strcpy(dest, src);
    }
};

template <typename CharTraits = DefaulsCharTraits>
class String
{
private:
    using char_t = typename CharTraits::char_t;
    char_t data[100];

public:
    String()
    {
        data[0] = 0;
    }

    String(char_t const *init)
    {
        CharTraits::copy(data, init);
    }

    bool operator==(String const &rhs)
    {
        String const &lhs = *this;
        size_t i;
        for (i = 0; lhs.data[i] != 0 && rhs.data[i] != 0; ++i)
            if (!CharTraits::equal(lhs.data[i], rhs.data[i]))
                return false;
        return lhs.data[i] == 0 && rhs.data[i] == 0;
    }
};

int main()
{
    String<SameCaseTraits> s1 = "hello";
    String<SameCaseTraits> s2 = "HeLLo";
    // Ha különbözőek akkor le sem fordul.
    std::cout << std::boolalpha << (s1 == s2) << std::endl;
}
