#include <cstring>
#include <iostream>

class string
{
private:
    char *c_str_;

public:
    explicit string(char c) : c_str_{new char[1 + 1]}
    {
        c_str_[0] = c;
        c_str_[1] = '\0';
    }

    explicit string(const char *c_str = "") : c_str_{new char[strlen(c_str) + 1]}
    {
        strcpy(c_str_, c_str);
    }

    explicit string(const string &other) : c_str_{new char[strlen(other.c_str_) + 1]}
    {
        strcpy(c_str_, other.c_str_);
    }

    string &operator=(char c)
    {
        delete[] c_str_;
        c_str_ = new char[1 + 1];
        c_str_[0] = c;
        c_str_[1] = '\0';
        return *this;
    }

    string &operator=(const char *c_str)
    {
        if (c_str_ != c_str)
        {
            delete[] c_str_;
            c_str_ = new char[strlen(c_str) + 1];
            strcpy(c_str_, c_str);
        }
        return *this;
    }

    string &operator=(const string &other)
    {
        if (this != &other)
        {
            delete[] c_str_;
            c_str_ = new char[strlen(other.c_str_) + 1];
            strcpy(c_str_, other.c_str_);
        }
        return *this;
    }

    string(char *&&c_str) noexcept : c_str_{c_str} {}

    string(string &&other) noexcept : c_str_{other.c_str_}
    {
        other.c_str_ = new char[1];
        other.c_str_[0] = '\0';
    }

    string &operator=(char *&&c_str) noexcept
    {
        if (c_str_ != c_str)
        {
            delete[] c_str_;
            c_str_ = c_str;
        }
        return *this;
    }

    string &operator=(string &&other) noexcept
    {
        if (this != &other)
        {
            delete[] c_str_;
            c_str_ = other.c_str_;
            other.c_str_ = new char[1];
            other.c_str_[0] = '\0';
        }
        return *this;
    }

    ~string()
    {
        delete[] c_str_;
    }

    string operator+(char c)
    {
        return *this + string{c};
    }

    string operator+(const char *c_str)
    {
        return *this + string{c_str};
    }

    string operator+(const string &other)
    {
        auto tmp = new char[strlen(c_str_) + strlen(other.c_str_) + 1];
        strcpy(tmp, c_str_);
        strcat(tmp, other.c_str_);
        return string{std::move(tmp)};
    }

    string &operator+=(char c)
    {
        return *this = *this + c;
    }

    string &operator+=(const char *c_str)
    {
        return *this = *this + c_str;
    }

    string &operator+=(const string &other)
    {
        return *this = *this + other;
    }

    bool operator<(const string &other)
    {
        return strcmp(c_str_, other.c_str_) < 0;
    }

    bool operator<=(const string &other)
    {
        return strcmp(c_str_, other.c_str_) <= 0;
    }

    bool operator==(const string &other)
    {
        return strcmp(c_str_, other.c_str_) == 0;
    }

    bool operator>=(const string &other)
    {
        return strcmp(c_str_, other.c_str_) >= 0;
    }

    bool operator>(const string &other)
    {
        return strcmp(c_str_, other.c_str_) > 0;
    }

    std::size_t length() const noexcept
    {
        return strlen(c_str_);
    }

    friend std::ostream &operator<<(std::ostream &os, const string &str);
};

std::ostream &operator<<(std::ostream &os, const string &str)
{
    return os << str.c_str_;
}

int main()
{
    auto leghosszabb = string{""};
    auto szo = string{""};
    auto c = int{};
    while ((c = std::cin.get()) != std::char_traits<char>::eof())
    {
        if (isalpha(c))
        {
            szo += c;
        }
        else
        {
            if (szo.length() > leghosszabb.length())
            {
                leghosszabb = std::move(szo);
            }
            szo = string{""};
        }
    }
    std::cout << leghosszabb << std::endl;
    return 0;
}