#include <stdio.h>
#include <math.h>

void clear(int H, int W, char page[H][W])
{
    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            page[y][x] = ' ';
        }
    }
}

void plot(int H, int W, char page[H][W], char c, double (*f)(double))
{
    for (int x = 0; x < W; ++x)
    {
        double fx = (x - W / 2) / 4.0;
        double fy = f(fx);
        int y = (fy * 4.0) * -1 + H / 2;
        if (y >= 0 && y < H)
        {
            page[y][x] = c;
        }
    }
}

void print(int H, int W, char page[H][W])
{
    for (int y = 0; y < H; ++y)
    {
        for (int x = 0; x < W; ++x)
        {
            putchar(page[y][x]);
        }
        putchar('\n');
    }
}

double d = 1.0;

double sin_d_times_x(double x)
{
    return sin(d * x);
}

int main()
{
    int W, H;
    printf("W = ?\n");
    scanf("%d", &W);
    printf("H = ?\n");
    scanf("%d", &H);

    char page[H][W];
    clear(H, W, page);

    printf("sin(d * x), d = ?\n");
    scanf("%lf", &d);

    plot(H, W, page, '.', sin_d_times_x);
    print(H, W, page);
    return 0;
}
