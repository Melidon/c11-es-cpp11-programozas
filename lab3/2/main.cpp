#include <iostream>
#include <cmath>
#include <functional>

class Page final
{
private:
    int H, W;
    char **page;

public:
    Page(int H, int W) : H{H}, W{W}
    {
        page = new char *[H];
        for (int i = 0; i < H; ++i)
        {
            page[i] = new char[W];
        }
        clear();
    }

    Page(const Page &) = delete;
    Page &operator=(const Page &) = delete;

    ~Page()
    {
        for (int i = 0; i < H; ++i)
        {
            delete page[i];
        }
        delete page;
    }

    int getH()
    {
        return H;
    }

    int getW()
    {
        return W;
    }

    void clear()
    {
        for (int y = 0; y < H; ++y)
        {
            for (int x = 0; x < W; ++x)
            {
                page[y][x] = ' ';
            }
        }
    }

    void print()
    {
        for (int y = 0; y < H; ++y)
        {
            for (int x = 0; x < W; ++x)
            {
                putchar(page[y][x]);
            }
            putchar('\n');
        }
    }

    void setchar(int y, int x, char c)
    {
        page[y][x] = c;
    }
};

template <typename Signature>
class Function;

template <typename Res, typename... ArgTypes>
class Function<Res(ArgTypes...)>
{
private:
    Res (*f)(ArgTypes...);

public:
    constexpr Function(Res (*f)(ArgTypes...)) : f{f} {}

    virtual Res operator()(ArgTypes... args) const
    {
        return std::invoke(f, args...);
    }
};

void plot(Page &page, char c, Function<double(double)> const &f)
{
    for (int x = 0; x < page.getW(); ++x)
    {
        double fx = (x - page.getW() / 2) / 4.0;
        double fy = f(fx);
        int y = (fy * 4.0) * -1 + page.getH() / 2;
        if (y >= 0 && y < page.getH())
        {
            page.setchar(y, x, c);
        }
    }
}

class Sin_d_times_x final : public Function<double(double)>
{
private:
    double d;

public:
    constexpr Sin_d_times_x(double d) : Function{nullptr}, d{d} {}

    double operator()(double x) const override
    {
        return sin(d * x);
    }
};

int main()
{
    int W, H;
    printf("W = ?\n");
    scanf("%d", &W);
    printf("H = ?\n");
    scanf("%d", &H);

    Sin_d_times_x f{0.5};

    Page p{H, W};
    plot(p, '*', f);
    p.print();

    return 0;
}
