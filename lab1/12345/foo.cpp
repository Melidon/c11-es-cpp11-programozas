void foo() {}

void foo(int i) {}

class Foo
{
public:
    void foo();
};
void Foo::foo() {}

namespace FooNS
{
    void foo() {}

    class Foo
    {
    public:
        void foo();
    };
    void Foo::foo() {}
}

int main(int argc, char *argv[])
{
    foo();
    foo(1);
    Foo().foo();
    FooNS::foo();
    FooNS::Foo().foo();
}
