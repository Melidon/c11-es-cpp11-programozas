#include <iostream>
#include <ranges>

class Element
{
public:
    Element *prev;
    Element *next;

public:
    Element() = default;
    ~Element()
    {
        delete next;
    }
    Element(const Element &) = delete;
    Element &operator=(const Element &) = delete;

    virtual void process(int) = 0;
};

class Dummy final : public Element
{
public:
    Dummy() = default;

    void process(int i) override
    {
        next->process(i);
    }
};

class Sieve final : public Element
{
private:
    const int n_;

public:
    Sieve(int n) : n_{n} {}

    void process(int i) override
    {
        if (i % n_ != 0)
        {
            next->process(i);
        }
    }
};

class Bucket final : public Element
{
public:
    Bucket() = default;

    void process(int i) override
    {
        std::cout << i << std::endl;
        auto sieve = new Sieve{i};
        sieve->next = this;
        sieve->prev = prev;
        prev->next = sieve;
        this->prev = sieve;
    }
};

int main()
{
    auto dummy = new Dummy{};
    auto bucket = new Bucket{};
    dummy->next = bucket;
    bucket->prev = dummy;
    using namespace std::views;
    for (auto i : iota(2, 100))
    {
        dummy->process(i);
    }
    delete dummy;
}
