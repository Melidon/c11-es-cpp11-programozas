#include <iostream>
#include <string>

std::string operator"" _s(const char *str, size_t len)
{
    return std::string{str, len};
}

int main(int, char **)
{
    std::cout << "hello"_s + " vilag" << std::endl;      /* hello vilag */
    std::cout << "hello\0vilag"_s.length() << std::endl; /* 11 */
}
