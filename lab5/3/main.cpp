#include <iostream>

auto operator"" _binary(const char *str)
{
    auto res = std::uint64_t{0};
    for (auto i = 0; str[i] != '\0'; ++i)
    {
        auto c = str[i];
        if (c != '0' && c != '1')
        {
            throw std::invalid_argument{"Only 1 and 0 values are accepted"};
        }
        res = 2 * res + c - '0';
    }
    return res;
}

int main(int, char **)
{
    std::cout << 1111_binary << std::endl;                 /* 15 */
    std::cout << 10000000_binary << std::endl;             /* 128 */
    std::cout << 10110111010111110111_binary << std::endl; /* 751095 */
}
