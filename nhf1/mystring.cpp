#include "mystring.h"

#include <cstring>
#include <exception>

MyString::Proxy::Proxy(MyString &str, std::size_t index)
    : str_{str},
      index_{index}
{
}

MyString::Proxy::operator char() const
{
    if (index_ >= str_.length())
    {
        throw std::out_of_range{"MyString::Proxy::operator char() const: index >= this->length()"};
    }
    return str_.stringValue_->c_str_[index_];
}

char MyString::Proxy::operator=(char c)
{
    if (index_ >= str_.length())
    {
        throw std::out_of_range{"MyString::Proxy::operator=(char c): index >= this->length()"};
    }
    str_.detach();
    return str_.stringValue_->c_str_[index_] = c;
}

char MyString::Proxy::operator=(const Proxy &other)
{
    return *this = static_cast<char>(other);
}

// Only for debugging.
long MyString::StringValue::instance_count = 0;

// Only for debugging.
long MyString::StringValue::get_instance_count()
{
    return instance_count;
}

MyString::StringValue::StringValue(char c)
    : c_str_{new char[1 + 1]}
{
    c_str_[0] = c;
    c_str_[1] = '\0';
    // Only for debugging.
    instance_count += 1;
}

MyString::StringValue::StringValue(const char *c_str)
    : c_str_{new char[strlen(c_str) + 1]}
{
    strcpy(c_str_, c_str);
    // Only for debugging.
    instance_count += 1;
}

MyString::StringValue::StringValue(char *&&c_str) noexcept
    : c_str_{c_str}
{
    c_str = nullptr;
    // Only for debugging.
    instance_count += 1;
}

MyString::StringValue::~StringValue()
{
    // Only for debugging.
    if (use_count_ > 0)
    {
        std::cout << "MyString::~StringValue(); c_str:\"" << c_str_ << "\"; use_count:" << use_count_ << std::endl;
    }
    delete c_str_;
    // Only for debugging.
    instance_count -= 1;
}

std::size_t MyString::StringValue::length() const
{
    return strlen(c_str_);
}

std::vector<MyString::StringValue *> MyString::stringValues_{};

MyString::StringValue *MyString::stringValueFrom(char c)
{
    for (auto stringValue : stringValues_)
    {
        if (stringValue->length() == 1 && stringValue->c_str_[0] == c)
        {
            return stringValue;
        }
    }
    auto stringValue = new StringValue{c};
    stringValues_.push_back(stringValue);
    return stringValue;
}

MyString::StringValue *MyString::stringValueFrom(const char *c_str)
{
    for (auto stringValue : stringValues_)
    {
        if (strcmp(stringValue->c_str_, c_str) == 0)
        {
            return stringValue;
        }
    }
    auto stringValue = new StringValue{c_str};
    stringValues_.push_back(stringValue);
    return stringValue;
}

MyString::StringValue *MyString::stringValueFrom(char *&&c_str)
{
    for (auto stringValue : stringValues_)
    {
        if (strcmp(stringValue->c_str_, c_str) == 0)
        {
            delete c_str;
            c_str = nullptr;
            return stringValue;
        }
    }
    auto stringValue = new StringValue{c_str};
    stringValues_.push_back(stringValue);
    c_str = nullptr;
    return stringValue;
}

// Only for debugging.
long MyString::get_StringValue_instance_count()
{
    return StringValue::get_instance_count();
}

inline void MyString::release_stringValue()
{
    if (stringValue_ == nullptr)
    {
        return;
    }
    stringValue_->use_count_ -= 1;
    if (stringValue_->use_count_ == 0)
    {
        delete stringValue_;
    }
}

inline void MyString::detach()
{
    if (stringValue_->use_count_ > 1)
    {
        stringValue_->use_count_ -= 1;
        stringValue_ = new StringValue{stringValue_->c_str_};
    }
}

MyString::MyString(char c)
    : stringValue_{stringValueFrom(c)}
{
}

MyString::MyString(const char *c_str)
    : stringValue_{stringValueFrom(c_str)}
{
}

MyString::MyString(StringValue *&&stringValue) noexcept
    : stringValue_{stringValue}
{
    stringValue = nullptr;
}

MyString::MyString(const MyString &other)
    : stringValue_{other.stringValue_}
{
    stringValue_->use_count_ += 1;
}

MyString::MyString(MyString &&other) noexcept
    : stringValue_{other.stringValue_}
{
    other.stringValue_ = nullptr;
}

MyString &MyString::operator=(char c)
{
    return *this = MyString{c};
}

MyString &MyString::operator=(const char *c_str)
{
    return *this = MyString{c_str};
}

MyString &MyString::operator=(const MyString &other)
{
    if (this != &other)
    {
        release_stringValue();
        stringValue_ = other.stringValue_;
        stringValue_->use_count_ += 1;
    }
    return *this;
}

MyString &MyString::operator=(MyString &&other) noexcept
{
    if (this != &other)
    {
        release_stringValue();
        stringValue_ = other.stringValue_;
        other.stringValue_ = nullptr;
    }
    return *this;
}

MyString::~MyString()
{
    release_stringValue();
}

MyString MyString::operator+(char c) const
{
    return *this + MyString{c};
}

MyString MyString::operator+(const char *c_str) const
{
    return *this + MyString{c_str};
}

MyString MyString::operator+(const MyString &other) const
{
    auto c_str = new char[strlen(stringValue_->c_str_) + strlen(other.stringValue_->c_str_) + 1];
    strcpy(c_str, stringValue_->c_str_);
    strcat(c_str, other.stringValue_->c_str_);
    auto stringValue = stringValueFrom(std::move(c_str));
    return MyString{std::move(stringValue)};
}

MyString &MyString::operator+=(char c)
{
    return *this += MyString{c};
}

MyString &MyString::operator+=(const char *c_str)
{
    return *this += MyString{c_str};
}

MyString &MyString::operator+=(const MyString &other)
{
    detach();
    return *this = *this + other;
}

std::size_t MyString::length() const
{
    return stringValue_->length();
}

// Only for debugging.
void MyString::printStringValueAddress(std::ostream &os) const
{
    const void *address = stringValue_;
    os << address << std::endl;
}

MyString::Proxy MyString::operator[](std::size_t index)
{
    return Proxy{*this, index};
}

const char &MyString::operator[](std::size_t index) const
{
    return stringValue_->c_str_[index];
}

std::ostream &operator<<(std::ostream &os, const MyString &str)
{
    os << str.stringValue_->c_str_;
    return os;
}

std::istream &operator>>(std::istream &is, MyString &str)
{
    static constexpr int buff_size = 10;
    char buff[buff_size];
    do
    {
        is.clear(is.rdstate() & ~std::ios::failbit);
        is.getline(buff, buff_size);
        str += buff;
    } while (is.fail());
    return is;
}
