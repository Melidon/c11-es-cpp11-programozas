#include <iostream>
#include <vector>

#include "solver.hpp"

void pitagoraszi_szamharmasok()
{
    Solver<int> solver;

    /* inicializáló lista helyett */
    std::vector<int> values;
    for (int i = 1; i <= 100; ++i)
    {
        values.push_back(i);
    }

    /* változók hozzáadása */
    auto a = solver.add_variable(values);
    auto b = solver.add_variable(values);
    auto c = solver.add_variable(values);

    /* feltételek hozzáadása */
    solver.add_constraint([a, b, c]
                          { return a() < b() && b() < c(); });
    solver.add_constraint([a, b, c]
                          { return a() * a() + b() * b() == c() * c(); });

    /* keresés és megoldások kiírása */
    int db = 0;
    solver.solve([&db]
                 { ++db; });
    std::cout << db << std::endl;
}

void euler_feladata()
{
    constexpr int allat_db = 100;
    constexpr int osszes_arany = 600;

    constexpr int sertes_arany = 21;
    constexpr int kecske_arany = 8;
    constexpr int juh_arany = 3;

    Solver<int> solver;

    /* változók hozzáadása */
    std::vector<int> sertes_lehetseges_dbk;
    for (int i = 0; i <= osszes_arany / sertes_arany; ++i)
        sertes_lehetseges_dbk.push_back(i);
    auto sertes_db = solver.add_variable(sertes_lehetseges_dbk);

    std::vector<int> kecske_lehetseges_dbk;
    for (int i = 0; i <= osszes_arany / kecske_arany; ++i)
        kecske_lehetseges_dbk.push_back(i);
    auto kecske_db = solver.add_variable(kecske_lehetseges_dbk);

    std::vector<int> juh_lehetseges_dbk;
    for (int i = 0; i <= osszes_arany / juh_arany; ++i)
        juh_lehetseges_dbk.push_back(i);
    auto juh_db = solver.add_variable(juh_lehetseges_dbk);

    /* feltételek hozzáadása */
    solver.add_constraint([sertes_db, kecske_db, juh_db]
                          { return sertes_db() + kecske_db() + juh_db() == allat_db; });
    solver.add_constraint([sertes_db, kecske_db, juh_db]
                          { return sertes_db() * sertes_arany + kecske_db() * kecske_arany + juh_db() * juh_arany == osszes_arany; });

    /* keresés és megoldások kiírása */
    solver.solve([sertes_db, kecske_db, juh_db]
                 { std::cout << "sertes_db=" << sertes_db() << ", kecske_db=" << kecske_db() << ", juh_db=" << juh_db() << std::endl; });
}

void terkep()
{
    Solver<std::string> solver;

    std::vector<std::string> szinek = {"kék", "sárga", "piros"};

    /* változók hozzáadása */
    auto A = solver.add_variable(szinek);
    auto B = solver.add_variable(szinek);
    auto C = solver.add_variable(szinek);
    auto D = solver.add_variable(szinek);
    auto E = solver.add_variable(szinek);

    /* feltételek hozzáadása */
    solver.add_constraint([A, B, C, D, E]
                          { return A() != B() && A() != C() && A() != D() && A() != E() && B() != C() && B() != D() && C() != E() && D() != E(); });
    solver.add_constraint([A, B, D, E]
                          { return A() > B() && D() < E(); });

    /* keresés és megoldások kiírása */
    solver.solve([A, B, C, D, E]
                 { std::cout << "A: " << A() << ", B: " << B() << ", C: " << C() << ", D: " << D() << ", E: " << E() << "." << std::endl; });
}

int main(int, char **)
{
    pitagoraszi_szamharmasok();
    euler_feladata();
    terkep();
}
