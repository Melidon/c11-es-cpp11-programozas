#pragma once

#include <functional>
#include <set>

#include "graph.hpp"

template <typename VertexData, typename EdgeData>
void depthFirstSearch(Graph<VertexData, EdgeData> &graph, typename Graph<VertexData, EdgeData>::Id from, std::function<void(VertexData &)> function)
{
    if (!function)
    {
        return;
    }

    auto visited = std::set<typename Graph<VertexData, EdgeData>::Id>{};

    std::function<void(typename Graph<VertexData, EdgeData>::Id)> depthFirstSearch_ = [&](typename Graph<VertexData, EdgeData>::Id id)
    {
        visited.insert(id);
        function(graph.get_vertex_data(id));
        for (auto edge : graph.get_vertex_edges(id))
        {
            if (!visited.contains(edge))
            {
                depthFirstSearch_(edge);
            }
        }
    };

    depthFirstSearch_(from);
}
