#pragma once

#include <cstdint>
#include <vector>

template <typename VertexData, typename EdgeData>
class Graph
{
public:
    using Id = std::uint64_t;

    [[nodiscard]] virtual Id add_vertex(VertexData data) = 0;
    [[nodiscard]] virtual VertexData &get_vertex_data(Id id) = 0;
    [[nodiscard]] virtual std::vector<Id> get_vertex_edges(Id from) = 0;
    virtual void add_edge(EdgeData data, Id from, Id to) = 0;
    [[nodiscard]] virtual EdgeData &get_edge_data(Id from, Id to) = 0;
};
