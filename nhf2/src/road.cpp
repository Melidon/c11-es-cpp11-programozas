#include "road.hpp"

Road::Road(double length)
    : length_{length}
{
}

double Road::get_length()
{
    return length_;
}
