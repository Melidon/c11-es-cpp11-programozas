#pragma once

class Road final
{
private:
    double length_;

public:
    [[nodiscard]] explicit Road(double length);

    [[nodiscard]] double get_length();
};
