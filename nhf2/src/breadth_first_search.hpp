#pragma once

#include <functional>
#include <set>
#include <queue>

#include "graph.hpp"

template <typename VertexData, typename EdgeData>
void breadthFirstSearch(Graph<VertexData, EdgeData> &graph, typename Graph<VertexData, EdgeData>::Id from, std::function<void(VertexData &)> function)
{
    if (!function)
    {
        return;
    }

    auto visited = std::set<typename Graph<VertexData, EdgeData>::Id>{};
    auto queue_ = std::queue<typename Graph<VertexData, EdgeData>::Id>{};
    queue_.push(from);
    while (!queue_.empty())
    {
        auto id = queue_.front();
        queue_.pop();
        visited.insert(id);
        function(graph.get_vertex_data(id));
        for (auto edge : graph.get_vertex_edges(id))
        {
            if (!visited.contains(edge))
            {
                queue_.push(edge);
            }
        }
    }
}
