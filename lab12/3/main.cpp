#include <iostream>

int greatest_v1(const int *arr, int size)
{
    int max = arr[0];
    for (int i = 1; i != size; ++i)
        if (arr[i] > max)
            max = arr[i];
    return max;
}

int greatest_v2(const int *arr, int size)
{
    const int head = arr[0];
    const int *tail = arr + 1;
    return size == 1 ? head : std::max(head, greatest_v2(tail, size - 1));
}

template <int head, int... tail>
struct Greatest_v3
{
    static constexpr int value = std::max(head, Greatest_v3<tail...>::value);
};

template <int head>
struct Greatest_v3<head>
{
    static constexpr int value = head;
};

int main()
{
    int a[] = {4, 87, 2, 65, 89, 1};
    std::cout << greatest_v1(a, 6) << std::endl;
    std::cout << greatest_v2(a, 6) << std::endl;
    std::cout << Greatest_v3<3, 9, 8, 5>::value << std::endl;
}
