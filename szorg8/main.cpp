#include <iostream>

struct X
{
    X() = default;
    X(X const &) { throw 0; }
};

void f_val(X param) noexcept
{
}

void f_ref(X const &param) noexcept
{
    X copy(param);
}

int main()
{
    X x1;

    try
    {
        std::cout << "f_val hivasa" << std::endl;
        // A kivétel a függvény előtt, a másolat készítésekor dobódik.
        f_val(x1);
    }
    catch (...)
    {
    }

    try
    {
        std::cout << "f_ref hivasa" << std::endl;
        /*
        Mivel referenciaként vettük át, ezért a kivétel akkor dobódik, amikor
        a függvény belsejében készítünk másolatot. Ez a függvény hazudott
        nekünk, ő valójában nem noexcept. Emiatt pedig rögtön megáll a program
        futása.
        */
        f_ref(x1);
    }
    catch (...)
    {
    }
    // Ami itt lenne az már nem történik meg.
}
