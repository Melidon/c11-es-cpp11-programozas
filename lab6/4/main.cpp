#include <cstdio>

class FilePtr final
{
private:
    FILE *fp_;

public:
    void close()
    {
        if (fp_ != nullptr)
        {
            fclose(fp_);
            fp_ = nullptr;
        }
    }
    explicit FilePtr() : fp_{nullptr} {}
    ~FilePtr()
    {
        close();
    }

    FilePtr(FILE *fp) : fp_{fp} {}
    FilePtr(const FilePtr &) = delete;
    FilePtr(FilePtr &&other) noexcept : fp_{other.fp_}
    {
        other.fp_ = nullptr;
    }

    FilePtr &operator=(FILE *fp)
    {
        close();
        fp_ = fp;
        return *this;
    }
    FilePtr &operator=(const FilePtr &) = delete;
    FilePtr &operator=(FilePtr &&other) noexcept
    {
        if (this != &other)
        {
            close();
            fp_ = other.fp_;
            other.fp_ = nullptr;
        }
        return *this;
    }

    operator FILE *()
    {
        return fp_;
    }
};

int fclose(FilePtr &) = delete;

int main()
{
    FilePtr fp;

    fp = fopen("hello.txt", "wt");
    fprintf(fp, "Hello vilag");
    // fclose(fp); // function "fclose(FilePtr &)" (declared at line 52) cannot be referenced -- it is a deleted function
}
