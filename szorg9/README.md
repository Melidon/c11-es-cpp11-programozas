# 9. szorgalmi

```C++
class String
{
    /* ... */
};

String::String(String &&moved) noexcept
{
    data_ = moved.data_;
    len_ = moved.len_;
    moved.data_ = new char[1];
    moved.data_[0] = '\0';
    moved.len_ = 0;
}
```
- A függvény azt állítja, hogy `noexcept`, de hazudik. A foglalás amúgy is felesleges. A mozgatott állapotban lévő osztálynak csak értékadhatónak és destruálhatónak kell lennie.
- Az osztály adattagjainak inicalizálására inicializáló listát kéne használni.

Ezek alapján az általam helyesnek gondolt kód:

```C++
class String
{
    /* ... */
};

String::String(String &&moved) noexcept
    : data_{moved.data_},
      len_{moved.len_}
{
    moved.data_ = nullptr;
    moved.len_ = 0; // Ezt én szívem szerint kihagynám, de előadáson az volt, hogy kell.
}
```
