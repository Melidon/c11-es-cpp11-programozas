#include <string_view>
#include <sstream>
#include <stack>
#include <functional>
#include <string>
#include <iostream>

/*
Igazából csak lemásoltam a 4.heti labor 3. feladatát
https://cpp11.eet.bme.hu/lab04/#3
Najó, utána átírtam, hogy lambdákat használjon.
*/
auto create_func_from_rpn(std::string_view sw)
{
    std::stringstream ss;
    ss << sw;

    // C++23
    std::stack<std::move_only_function<double(double)>> rpn{};
    std::string word;
    while (ss >> word)
    {
        if (word == "+")
        {
            // Amúgy nem értem. Miért lehet itt lehagyni az `std::`-ot a `move` elől?
            auto e1 = move(rpn.top());
            rpn.pop();
            auto e2 = move(rpn.top());
            rpn.pop();
            rpn.emplace([e2 = move(e2), e1 = move(e1)](double x) mutable // `Az std::move_only_function`-nek valamiért nincsen `operator() const`-ja.
                        { return e2(x) + e1(x); });
        }
        else if (word == "*")
        {
            auto e1 = move(rpn.top());
            rpn.pop();
            auto e2 = move(rpn.top());
            rpn.pop();
            rpn.emplace([e2 = move(e2), e1 = move(e1)](double x) mutable
                        { return e2(x) * e1(x); });
        }
        else if (word == "x")
        {
            rpn.emplace([](double x)
                        { return x; });
        }
        else
        {
            // Itt szintén le tudom hagyni az `std::`-ot a `stod` elől...
            rpn.emplace([c = stod(word)](double)
                        { return c; });
        }
    }
    return move(rpn.top());
}

int main(int, char **)
{
    auto f = create_func_from_rpn("21.7 34.2 x * +"); // f = 21.7+34.2*x

    std::cout << f(3) << std::endl; // 21.7+34.2*3 értéke
    std::cout << f(4) << std::endl; // 21.7+34.2*4 értéke
    std::cout << f(5) << std::endl; // 21.7+34.2*5 értéke
}
